package org.stdx.javac;

public class AnInterfaceImplementation implements AnInterface {

	@Override
	public String aMethod(int aMethodParam) {
		return String.valueOf(aMethodParam);
	}

}
