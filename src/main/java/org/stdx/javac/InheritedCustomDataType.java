package org.stdx.javac;

public class InheritedCustomDataType extends CustomDataType {

	private byte anotherByte;

	public byte getAnotherByte() {
		return anotherByte;
	}

	public void setAnotherByte(byte anotherByte) {
		this.anotherByte = anotherByte;
	}

}
