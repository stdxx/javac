package org.stdx.javac;

public class MutableOpenClass {

	private int thePrimitiveInteger;
	private Integer theInteger;
	private double thePrimitveDouble;
	private Double theDouble;
	private String theString;
	private byte thePrimitiveByte;
	private Byte theByte;
	private CustomDataType customDataType;

	public CustomDataType getCustomDataType() {
		return customDataType;
	}

	public Byte getTheByte() {
		return theByte;
	}

	public Double getTheDouble() {
		return theDouble;
	}

	public Integer getTheInteger() {
		return theInteger;
	}

	public byte getThePrimitiveByte() {
		return thePrimitiveByte;
	}

	public int getThePrimitiveInteger() {
		return thePrimitiveInteger;
	}

	public double getThePrimitveDouble() {
		return thePrimitveDouble;
	}

	public String getTheString() {
		return theString;
	}

	public void setCustomDataType(CustomDataType customDataType) {
		this.customDataType = customDataType;
	}

	public void setTheByte(Byte theByte) {
		this.theByte = theByte;
	}

	public void setTheDouble(Double theDouble) {
		this.theDouble = theDouble;
	}

	public void setTheInteger(Integer theInteger) {
		this.theInteger = theInteger;
	}

	public void setThePrimitiveByte(byte thePrimitiveByte) {
		this.thePrimitiveByte = thePrimitiveByte;
	}

	public void setThePrimitiveInteger(int thePrimitiveInteger) {
		this.thePrimitiveInteger = thePrimitiveInteger;
	}

	public void setThePrimitveDouble(double thePrimitveDouble) {
		this.thePrimitveDouble = thePrimitveDouble;
	}

	public void setTheString(String theString) {
		this.theString = theString;
	}
}
