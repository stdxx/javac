package org.stdx.javac;

public final class MutableFinalClass {

	private final int thePrimitiveInteger;
	private final String theString;

	public MutableFinalClass(int thePrimitiveInteger, String theString) {
		this.thePrimitiveInteger = thePrimitiveInteger;
		this.theString = theString;
	}

	public int getThePrimitiveInteger() {
		return thePrimitiveInteger;
	}

	public String getTheString() {
		return theString;
	}

}
